// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
// Initial and Config Server.
const app = express();
const server = http.Server(app);
const socketServer = require('socket.io')(server);

const config = require('./config');
// Mongoose setup
mongoose.Promise = global.Promise;
if (process.env.DB_CONNECTION || config.DB_CONNECTION) {
    mongoose.connect(process.env.DB_CONNECTION || config.DB_CONNECTION, { useNewUrlParser: true });
    const db = mongoose.connection;
    db.on('error', function (err) {
        console.error(err);
    });
    db.once('open', function callback() {
        console.info('Connected to the database');
    });
} else {
    console.log('DB connection string was not found.');
}

// Parsers for POST data
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


// Point static path to "build"
app.use(express.static(path.join(__dirname, 'dist')));

// Get our API routes
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,authorization');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});
const routes = require('./api/v1/routes/routes')(express);

// Web Socket Handler
//const chatServerHandler = require('./api/v1/ws/chat-server.socket').chatServerHandler;

// Set our api routes
app.use('/api/v1', routes);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});
// Initialize Socket Server
require('./api/ws/chat-server')(socketServer);

/*** Get port from environment and store in Express.*/
const port = process.env.PORT || 3000;
app.set('port', port);

server.listen(port, () => console.log(`Our server is running on: ${port}`));