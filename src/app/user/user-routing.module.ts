import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
// Components
import { UserHomeComponent } from './home/home.component';
import { UserChatComponent } from './chat/chat.component'; 
// Guard
import { LoggedInGuardService } from '../shared/guards/authorized.guard';
const appRoutes: Routes = [
  // Authenticated  
  { path: 'user/home', component: UserHomeComponent, canActivate: [LoggedInGuardService]},
  { path: 'user/chat', component: UserChatComponent, canActivate: [LoggedInGuardService]},
  { path: 'user/main', redirectTo: '/user/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
  exports: [RouterModule],
})
export class UserRoutingModule {
}
