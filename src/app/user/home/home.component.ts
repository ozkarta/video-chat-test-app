import { Component, OnInit, OnDestroy } from '@angular/core';
import { SpinnerService } from 'src/app/shared/services/spinner.service';
@Component({
  selector: 'app-user-home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class UserHomeComponent implements OnInit, OnDestroy {
  constructor(
    private spinnerService: SpinnerService
  ) {}

  ngOnInit() {

  }

  ngOnDestroy() {

  }

  initialLoad() {
    this.spinnerService.next(false);
  }
}