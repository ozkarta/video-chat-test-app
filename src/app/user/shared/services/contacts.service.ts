import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { UserModel } from '../../../shared/models/user.model';

@Injectable()
export class ContactsService {
  constructor(private http: HttpClient) {
  }

  getContacts(): Observable<UserModel[]> {
    return this.http.get<UserModel[]>('/api/v1/user/contacts')
      .pipe(
        map((users: UserModel[]) => {
          return users;
        }),
        catchError((error: Response) => {
          return throwError(error);
        }));
  }
}