import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ChatWSService {
  private wsMessageBody: any = { authorization: this.getCookie('access_token')};
  constructor(private socket: Socket) {
    this.tokenNeededListener();
  }

  public sendMessage(from: string, to: string, message: string) {
    let wsMessageBodyCP = Object.assign({}, this.wsMessageBody);
    wsMessageBodyCP.from = from;
    wsMessageBodyCP.to = to;
    wsMessageBodyCP.message = message;
    this.socket.emit('message', wsMessageBodyCP);
  }

  private tokenNeededListener() {
    this.socket.on('we_need_your_token', () => {
      this.socket.emit('you_got_my_token', this.wsMessageBody);
    })
  }

  public newMessageObservable(): Observable<any> {
    return <any>(this.socket.fromEvent('message'));
  }

  public requestP2P(applicant: string, targetUserId: string) {
    let wsMessageBodyCP = Object.assign({}, this.wsMessageBody);
    wsMessageBodyCP.target = targetUserId;
    wsMessageBodyCP.applicant = applicant;
    this.socket.emit('I_WANT_P2P', wsMessageBodyCP);
  }

  public P2PRequesObservable(): Observable<any> {
    return <any>this.socket.fromEvent('NEW_P2P_REQUEST');
  }

  public P2PDenialObservable(): Observable<any> {
    return <any>this.socket.fromEvent('P2P_DENIAL');
  }

  public P2PAcceptanceObservable(): Observable<any> {
    return <any>this.socket.fromEvent('P2P_ACCEPTANCE');
  }

  public P2PCancellationObservable(): Observable<any> {
    return <any>this.socket.fromEvent('P2P_CANCELLATION');
  }

  public getPeerObservable(): Observable<any> {
    return <any>this.socket.fromEvent('GET_PEER');
  }

  public sendP2PDenialTo(applicant: string, targetUserId: string) {
    let wsMessageBodyCP = Object.assign({}, this.wsMessageBody);
    wsMessageBodyCP.applicant = applicant;
    wsMessageBodyCP.target = targetUserId;
    this.socket.emit('P2P_DENIAL', wsMessageBodyCP);
  }

  public sendP2PAcceptanceTo(applicant: string, targetUserId: string) {
    let wsMessageBodyCP = Object.assign({}, this.wsMessageBody);
    wsMessageBodyCP.applicant = applicant;
    wsMessageBodyCP.target = targetUserId;
    this.socket.emit('P2P_ACCEPTANCE', wsMessageBodyCP);
  }

  public sendP2PCancellationTo(applicant: string, targetUserId: string) {
    let wsMessageBodyCP = Object.assign({}, this.wsMessageBody);
    wsMessageBodyCP.applicant = applicant;
    wsMessageBodyCP.target = targetUserId;
    this.socket.emit('P2P_CANCELLATION', wsMessageBodyCP);
  }

  public sendPeerToTarget(targetId: string, peer: any) {
    let wsMessageBodyCP = Object.assign({}, this.wsMessageBody);
    wsMessageBodyCP.target = targetId;
    wsMessageBodyCP.peer = peer;
    this.socket.emit('SEND_PEER', wsMessageBodyCP);
  }

  private getCookie(cName: string): any {
    if (localStorage.getItem(cName)) {
      return decodeURI(localStorage.getItem(cName) || '');
    }
    return '';
  }
}