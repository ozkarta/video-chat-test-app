import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';


@Injectable()
export class ChatService {
  constructor(private http: HttpClient) {
  }

  getChat(friendId: string): Observable<any> {
    return this.http.get<any>(`/api/v1/user/chats/${friendId}`)
      .pipe(
        map((chat: any) => {
          return chat;
        }),
        catchError((error: Response) => {
          return throwError(error);
        }));
  }
}