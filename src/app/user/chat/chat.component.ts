import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { SpinnerService } from '../../shared/services/spinner.service';
import { ContactsService } from '../shared/services/contacts.service';
import { UserModel } from '../../shared/models/user.model';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError, Subscription } from 'rxjs';
import { ChatService } from '../shared/services/chat.service';
import { ChatWSService } from '../shared/services/chat.ws.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { VideoCallModalComponent } from './video-call-modal/video-call-modal.component';
import { RingingModalComponent } from './ringing-modal/ringing-modal.component';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'app-user-chat-component',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})

export class UserChatComponent implements OnInit, OnDestroy {
  @ViewChild('scrollableMessages') scrollableMessages: ElementRef = <any>null;
  private subscription: Subscription = new Subscription();
  public me: UserModel = <any>null;
  public contacts: any[] = [];
  public sendBoxModel: string = '';
  public selectedChat: any = null;
  constructor(
    private authService: AuthService,
    private spinnerService: SpinnerService,
    private contactsService: ContactsService,
    private chatService: ChatService,
    private chatWSService: ChatWSService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.authService.getUser.subscribe(
      (me: UserModel) => {
        this.me = me;
        if (!this.me) {
          return;
        }
        this.initialLoad();

        this.subscription.add(this.chatWSService.newMessageObservable()
        .subscribe(
          (data: any) => {
            console.dir(data);
            let message = data.message;
            let chat = data.chat;
            let user = data.user;
            this.incommingMessageProcessor(user, chat, message);
          }
        ));

        this.subscription.add(this.chatWSService.P2PRequesObservable()
        .subscribe(
          (data: any) => {
            console.log(data);
            this.openRingingDialog()
              .then(() => {
                this.chatWSService.sendP2PAcceptanceTo(data.from, this.me._id || '');
                this.openVideoCallDialog(data.from, true);
              })
              .catch((declined) => {
                if (declined) {
                  this.chatWSService.sendP2PDenialTo(data.from, this.me._id || '');
                }
              });
          }
        ));
      }
    );
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  initialLoad() {
    this.spinnerService.next(true);
    this.loadContactsObservable()
      .subscribe(
        (success: any) => {
          console.dir(success);
          this.spinnerService.next(false);
        },
        (error: Error) => {
          console.dir(error);
        }
      );
  }

  loadContactsObservable(): Observable<any> {
    return this.contactsService.getContacts()
      .pipe(
        map((contacts: UserModel[]) => {
          this.contacts = contacts.map((contact: UserModel) => {
            if (contact.gender === 'MALE') {
              contact.profileImage = 'https://ptetutorials.com/images/user-profile.png';
            }

            if (contact.gender === 'FEMALE') {
              contact.profileImage = 'http://www.clker.com/cliparts/j/0/9/Q/D/y/red-female-icon-hi.png';
            }

            if (!contact.profileImage) {
              contact.profileImage = 'https://assets.vice.com/content-images/contentimage/no-slug/d1eb42b12d244b098ca23ee7b677c812.jpg'
            }

            contact.active = false;
            return contact;
          });
          return contacts;
        }),
        catchError((error: Response) => {
          return throwError(error);
        })
      );
  }

  // Event Handlers
  contactClickHandler(_contact: any) {
    this.sendBoxModel = '';
    let activeContact: any = null;
    this.contacts.forEach((contact: any) => {
      if (contact !== _contact) {
        contact.active = false;
      } else {
        contact.active = true;
        activeContact = contact;
      }
    });

    if (activeContact) {
      this.spinnerService.next(true);
      this.chatService.getChat(activeContact._id)
        .subscribe(
          (chat: any) => {
            // this.selectedChat = chat;
            activeContact.chat = chat;
            this.spinnerService.next(false);
          },
          (error: Error) => {
            console.dir(error);
            this.spinnerService.next(false);
          }
        )
    }
  }

  sendMessage() {
    this.chatWSService.sendMessage(this.me._id || '', this.getActiveContact()['_id'], this.sendBoxModel)
    this.sendBoxModel = '';
  }

  videoCall() {
    let userId = this.getActiveContact()._id;
    this.openVideoCallDialog(userId);
  }

  // Helpers
  getActiveContact() {
    let contact = null;
    for (let i = 0; i < this.contacts.length; i++) {
      if (this.contacts[i].active) {
        contact = this.contacts[i];
      }
    }
    return contact;
  }

  getMessageSender(message: any) {
    let contact = null;
    for (let i = 0; i < this.contacts.length; i++) {
      if (this.contacts[i]._id === message.sender) {
        contact = this.contacts[i];
      }
    }

    return contact;
  }

  getActiveContactMessages() {
    let contact = this.getActiveContact();
    if (contact && contact.messages) {
      return contact.messages
    } else {
      return [];
    }
  }

  isMessageIncomming(message: any) {
    return !(message.sender === this.me._id);
  }

  isMessageOutgoing(message: any) {
    return message.sender === this.me._id;
  }

  incommingMessageProcessor(user: any, chat: any, message: any) {
    let contactInAction: any = null;
    this.contacts.forEach((contact: any) => {
      if (contact._id === user._id) {
        contactInAction = contact;
      }
    });

    if (contactInAction) {
      if (contactInAction.chat) {
        contactInAction.chat.messages.push(message);
      } else {
        chat.messages = [message];
        contactInAction.chat = chat;
      }
    } else {
      chat.messages = [message];
      user.chat = chat;
      this.contacts.unshift(user);
    }

    setTimeout(() => {
      this.scrollMessagesToButtom();
    }, 100);
  }

  scrollMessagesToButtom() {
    try {
      this.scrollableMessages.nativeElement.scrollTop = this.scrollableMessages.nativeElement.scrollHeight;
    } catch (exeption) {
      console.dir(exeption);
    }
  }

  openVideoCallDialog(targetId: string, answeringMode: boolean = false): void {
    const dialogRef = this.dialog.open(VideoCallModalComponent, {
      width: '900px',
      data: { targetId: targetId, me: this.me._id, answeringMode: answeringMode }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('closed');
      if (!result || !result.remote) {
        this.chatWSService.sendP2PCancellationTo(this.me._id || '', targetId);
      } else {
        console.log('Remote');
      }
    });
  }

  openRingingDialog(): Promise<any> {
    const dialogRef = this.dialog.open(RingingModalComponent, {
      width: '500px',
      data: {}
    });
    return new Promise((resolve, reject) => {
      dialogRef.afterClosed().subscribe(result => {
        console.log(result);
        if (result.cancel) {
          return reject(false);
        }
        if (result.accept) {
          return resolve(true);
        }
        return reject(true);
      });
    });    
  }
}