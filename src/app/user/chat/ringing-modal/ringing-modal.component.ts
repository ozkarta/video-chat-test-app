import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ChatWSService } from '../../shared/services/chat.ws.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-ringing-modal-component',
  templateUrl: './ringing-modal.component.html',
  styleUrls: ['./ringing-modal.component.css']
})
export class RingingModalComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();

  constructor(
    private chatWSService: ChatWSService,
    public dialogRef: MatDialogRef<RingingModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.subscription.add(this.chatWSService.P2PCancellationObservable()
    .subscribe(
      (data) => {
        console.log('Should Be Closed');
        console.log(data);
        this.dialogRef.close({cancel: true});
      }
    ));
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onNoClick(): void {
    this.dialogRef.close({decline: true});
  }

  onYesClick(): void {
    this.dialogRef.close({accept: true});
  }
}