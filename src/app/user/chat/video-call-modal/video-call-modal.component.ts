import { Component, Inject, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ChatWSService } from '../../shared/services/chat.ws.service';
import { Subscription, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-video-call-modal-component',
  templateUrl: './video-call-modal.component.html',
  styleUrls: ['./video-call-modal.component.css']
})
export class VideoCallModalComponent implements OnInit, OnDestroy{
  private targetpeer: any;
  private peer: any;
  private signalData: any = null;
  @ViewChild('myvideo') myVideo: any;
  private n = <any>navigator;
  private subscription: Subscription = new Subscription();
  private stream: any = new MediaStream();
  private video: any;
  private isPeerReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  constructor(
    private chatWSService: ChatWSService,
    public dialogRef: MatDialogRef<VideoCallModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() { 
    this.video = this.myVideo.nativeElement;
    this.n.getUserMedia = (this.n.getUserMedia || this.n.webkitGetUserMedia || this.n.mozGetUserMedia || this.n.msGetUserMedia);
    

    this.getStreamTracks()
      .then(
        (tracks: any) => {
          if (tracks && tracks.length) {
            tracks.forEach((track: any) => {
              this.stream.addTrack(track);
            });
          }
          this.createPeer();
        }
      )

    this.initSubscriptions();

  }

  ngOnDestroy() {
    if (this.peer) {
      this.peer.destroy();
    }

    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.stream) {
      this.stream.getTracks().forEach((trackle: any) => {
        console.log(trackle);
        if (typeof trackle.stop === 'function') {
          trackle.stop();
        }
      });
      if (typeof this.stream.stop === 'function') {
        this.stream.stop();
      }      
    }
  }

  getStreamTracks() {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getStreamTrack({audio: true}),
        this.getStreamTrack({video: true})
      ]).then(
        (data: any) => {
          let result: any[] = [];
          data.forEach((trackleArray: any[]) => {
            result = [...result, ...trackleArray];
          });
          return resolve(result);
        }
      ).catch((error: any) => {
        return resolve([]);
      })
    });
  }

  getStreamTrack(options: any) {
    return new Promise((resolve, reject) => {
      this.n.getUserMedia(options, (stream: any) =>  {
        return resolve(stream.getTracks());   
      }, (error: any) => {
        console.log(error);
        return resolve([]);
      });
    })
  }

  createPeer() {
    this.peer = new SimplePeer({
      initiator: !this.data.answeringMode,
      trickle: false,
      stream: this.stream,
    });

    if (!this.data.answeringMode) {
      this.chatWSService.requestP2P(this.data.me, this.data.targetId);      
    }  

    //_________________________________________
    this.peer.on('connect', () => {
      console.log('connect');
    });

    this.peer.on('data', (data: any) => {
      console.log('data');
    });

    this.peer.on('error', (error: any) => {
      console.log('error');
      console.log(error);
    });

    this.peer.on('signal', (data: any) => {
      console.log('signal');
      this.signalData = data;
      if (this.data.answeringMode) {
        this.chatWSService.sendPeerToTarget(this.data.targetId, this.signalData);
      }
    });

    this.peer.on('stream', (stream: any) => {
      console.log('Incomming stream...');
      this.video.srcObject = stream;
      this.video.play();
    });
    //_____________________________________________________________________________
    this.isPeerReady.next(true);

    console.log(this.peer);
  }

  initSubscriptions() {
    this.subscription.add(this.chatWSService.P2PDenialObservable()
      .subscribe(
        (data) => {
          this.onNoClick();
        }
      ));
    
    this.subscription.add(this.chatWSService.P2PAcceptanceObservable()
      .subscribe(
        (data) => {
          console.log('Accepted Your Call');
          this.chatWSService.sendPeerToTarget(this.data.targetId, this.signalData);
        }
      ));

    this.subscription.add(this.chatWSService.P2PCancellationObservable()
    .subscribe(
      (data) => {
        console.log('Should Be Closed');
        this.dialogRef.close({ remote: true });
      }
    ));

    this.subscription.add(this.chatWSService.getPeerObservable()
    .subscribe(
      (response) => {
        this.targetpeer = response.peer;
        console.log(this.targetpeer);
        this.subscription.add(this.isPeerReady.subscribe(
          (ready) => {
            if (ready) {
              this.peer.signal(this.targetpeer);
            }
          }
        ));
      }
    ));
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}