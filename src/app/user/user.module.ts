import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Module Imports
import { UserRoutingModule } from './user-routing.module';

// Components
import { UserHomeComponent } from './home/home.component';
import { UserChatComponent } from './chat/chat.component'; 
import { VideoCallModalComponent } from './chat/video-call-modal/video-call-modal.component';
import { RingingModalComponent } from './chat/ringing-modal/ringing-modal.component';

// Services
import { ContactsService } from './shared/services/contacts.service';
import { ChatService } from './shared/services/chat.service';
import { ChatWSService } from './shared/services/chat.ws.service';
@NgModule({
  declarations: [
    UserHomeComponent,
    UserChatComponent,
    VideoCallModalComponent,
    RingingModalComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
		HttpClientModule,
		
		UserRoutingModule
  ],
  providers: [
    ContactsService,
    ChatService,
    ChatWSService,
  ],
  entryComponents: [
    VideoCallModalComponent,
    RingingModalComponent,
  ]
})
export class UserModule { }
