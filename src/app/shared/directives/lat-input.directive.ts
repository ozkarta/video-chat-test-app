import { Directive, HostListener, Output, EventEmitter, ElementRef } from '@angular/core';

@Directive({
  selector: '[LatInputDirective]'
})
export class LatInputDirective {
  public obj: any = {
    '65': {
      originalChar: 'a',
      geo: {
        shift: 'ა',
        noShift: 'ა'
      },
      lat: {
        shift: 'A',
        noShift: 'a'
      }
    },
    '66': {
      originalChar: 'b',
      geo: {
        shift: 'ბ',
        noShift: 'ბ'
      },
      lat: {
        shift: 'B',
        noShift: 'b'
      }
    },
    '67': {
      originalChar: 'c',
      geo: {
        shift: 'ჩ',
        noShift: 'ც'
      },
      lat: {
        shift: 'C',
        noShift: 'c'
      }
    },
    '68': {
      originalChar: 'd',
      geo: {
        shift: 'დ',
        noShift: 'დ'
      },
      lat: {
        shift: 'D',
        noShift: 'd'
      }
    },
    '69': {
      originalChar: 'e',
      geo: {
        shift: 'ე',
        noShift: 'ე'
      },
      lat: {
        shift: 'E',
        noShift: 'e'
      }
    },
    '70': {
      originalChar: 'f',
      geo: {
        shift: 'ფ',
        noShift: 'ფ'
      },
      lat: {
        shift: 'F',
        noShift: 'f'
      }
    },
    '71': {
      originalChar: 'g',
      geo: {
        shift: 'გ',
        noShift: 'გ'
      },
      lat: {
        shift: 'G',
        noShift: 'g'
      }
    },
    '72': {
      originalChar: 'h',
      geo: {
        shift: 'ჰ',
        noShift: 'ჰ'
      },
      lat: {
        shift: 'H',
        noShift: 'h'
      }
    },
    '73': {
      originalChar: 'i',
      geo: {
        shift: 'ი',
        noShift: 'ი'
      },
      lat: {
        shift: 'I',
        noShift: 'i'
      }
    },
    '74': {
      originalChar: 'j',
      geo: {
        shift: 'ჟ',
        noShift: 'ჯ'
      },
      lat: {
        shift: 'J',
        noShift: 'j'
      }
    },
    '75': {
      originalChar: 'k',
      geo: {
        shift: 'კ',
        noShift: 'კ'
      },
      lat: {
        shift: 'K',
        noShift: 'k'
      }
    },
    '76': {
      originalChar: 'l',
      geo: {
        shift: 'ლ',
        noShift: 'ლ'
      },
      lat: {
        shift: 'L',
        noShift: 'l'
      }
    },
    '77': {
      originalChar: 'm',
      geo: {
        shift: 'მ',
        noShift: 'მ'
      },
      lat: {
        shift: 'M',
        noShift: 'm'
      }
    },
    '78': {
      originalChar: 'n',
      geo: {
        shift: 'ნ',
        noShift: 'ნ'
      },
      lat: {
        shift: 'N',
        noShift: 'n'
      }
    },
    '79': {
      originalChar: 'o',
      geo: {
        shift: 'ო',
        noShift: 'ო'
      },
      lat: {
        shift: 'O',
        noShift: 'o'
      }
    },
    '80': {
      originalChar: 'p',
      geo: {
        shift: 'პ',
        noShift: 'პ'
      },
      lat: {
        shift: 'P',
        noShift: 'p'
      }
    },
    '81': {
      originalChar: 'q',
      geo: {
        shift: 'ქ',
        noShift: 'ქ'
      },
      lat: {
        shift: 'Q',
        noShift: 'q'
      }
    },
    '82': {
      originalChar: 'r',
      geo: {
        shift: 'ღ',
        noShift: 'რ'
      },
      lat: {
        shift: 'R',
        noShift: 'r'
      }
    },
    '83': {
      originalChar: 's',
      geo: {
        shift: 'შ',
        noShift: 'ს'
      },
      lat: {
        shift: 'S',
        noShift: 's'
      }
    },
    '84': {
      originalChar: 't',
      geo: {
        shift: 'თ',
        noShift: 'ტ'
      },
      lat: {
        shift: 'T',
        noShift: 't'
      }
    },
    '85': {
      originalChar: 'u',
      geo: {
        shift: 'უ',
        noShift: 'უ'
      },
      lat: {
        shift: 'U',
        noShift: 'u'
      }
    },
    '86': {
      originalChar: 'v',
      geo: {
        shift: 'ვ',
        noShift: 'ვ'
      },
      lat: {
        shift: 'V',
        noShift: 'v'
      }
    },
    '87': {
      originalChar: 'w',
      geo: {
        shift: 'ჭ',
        noShift: 'წ'
      },
      lat: {
        shift: 'W',
        noShift: 'w'
      }
    },
    '88': {
      originalChar: 'x',
      geo: {
        shift: 'ხ',
        noShift: 'ხ'
      },
      lat: {
        shift: 'X',
        noShift: 'x'
      }
    },
    '89': {
      originalChar: 'y',
      geo: {
        shift: 'ყ',
        noShift: 'ყ'
      },
      lat: {
        shift: 'Y',
        noShift: 'y'
      }
    },
    '90': {
      originalChar: 'z',
      geo: {
        shift: 'ძ',
        noShift: 'ზ'
      },
      lat: {
        shift: 'Z',
        noShift: 'z'
      }
    },
  }
  @Output() ngModelChange:EventEmitter<any> = new EventEmitter()
  private el: any;
  constructor(el: ElementRef) {
    this.el = el.nativeElement
  }

  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('copy', ['$event']) blockCopy(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('cut', ['$event']) blockCut(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('drop', ['$event']) blockDrop(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('keydown', ['$event'])
  onInput(event: any) {
    let result = '';
    if (!this.obj[event.keyCode] || !this.obj[event.keyCode]['lat']) {
    	return;
    }
    if (event.shiftKey) {
    	result = this.obj[event.keyCode]['lat']['shift'];
    } else {
    	result = this.obj[event.keyCode]['lat']['noShift'];
    }
    this.el.value += result;
    this.ngModelChange.emit(this.el.value);
    event.preventDefault();  
  }

  @HostListener('contextmenu', ['$event']) mouseEvent(e: MouseEvent) {
    e.stopPropagation();
    e.preventDefault();
  }
}