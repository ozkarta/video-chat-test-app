import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs';

export class AddHeaderInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Clone the request to add the new header
    const clonedRequest = req.clone({ setHeaders: { authorization: this.getCookie('access_token')} });

    //const clonedRequest = req.clone({});
    // Pass the cloned request instead of the original request to the next handle
    return next.handle(clonedRequest);
  }

  getCookie(cName: string): any {
    if (localStorage.getItem(cName)) {
      return decodeURI(localStorage.getItem(cName) || '');
    }
    return '';
  }
}