import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
@Component({
  selector: 'app-alert-dialog',
  templateUrl: 'alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.style.css']
})
export class AlertDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<AlertDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close({cancel: true});
	}
	
	onYesClick(): void {
    this.dialogRef.close({submit: true});
  }

}