

export class UserModel {
  public firstName: string = '';
  public lastName: string = '';
  public gender: string = '';
  public birthDate: Date = new Date();
  public email: string = '';
  public password: string = '';
  public profileImage?: string;
  public active?: boolean;
  public _id?: string;
  constructor() {

  }
}