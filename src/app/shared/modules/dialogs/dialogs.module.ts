import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AlertDialogComponent } from '../../components/dialogs/alert-dialog/alert-dialog.component';
import { ConfirmationDialogComponent } from '../../components/dialogs/confirmation-dialog/confirmation-dialog.component';
import { ImageViewComponent } from '../../components/dialogs/image-view-component/image-view.component';

@NgModule({
  declarations: [
    AlertDialogComponent,
    ConfirmationDialogComponent,
    ImageViewComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
		HttpClientModule,
		RouterModule
	],
	exports: [
    AlertDialogComponent,
    ConfirmationDialogComponent,
    ImageViewComponent,
	],
  providers: [
  ],
  entryComponents: [
    AlertDialogComponent,
    ConfirmationDialogComponent,
    ImageViewComponent,
  ]
})
export class DialogsModule { }
