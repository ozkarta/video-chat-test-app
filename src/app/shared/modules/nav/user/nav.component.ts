import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../services/auth.service';
@Component({
	selector: 'app-user-navbar',
	templateUrl: './nav.component.html',
	styleUrls: ['./nav.component.css'],
})

export class UserNavbar implements OnInit, OnDestroy{
	public pageYOffset: number = 0;
	private routeUrlSubscription: Subscription = new Subscription();
	public currentUrl = '';

	constructor(private route: ActivatedRoute,
							private authService: AuthService,
						  private router: Router) {

	}

	ngOnInit() {
		// this.subscribeUrlParameters();
		// this.subscribeRouter();
	}

	ngOnDestroy() {
		if (this.routeUrlSubscription) {
			this.routeUrlSubscription.unsubscribe();
		}		
	}


  subscribeUrlParameters() {
    this.routeUrlSubscription = this.route.params.subscribe(params => {
      console.log(params);
    });
	}
	
	subscribeRouter() {
		this.router.events.subscribe((event: any) => {
			if (event instanceof NavigationEnd) {
				this.currentUrl = (<NavigationEnd>event).urlAfterRedirects;
			} 
		})
	}

	logOutClickHandler(event?: Event) {
		this.authService.logOut()
			.subscribe(
				(result: boolean) => {
					this.router.navigate(['/']);
				}
			);
	}

}