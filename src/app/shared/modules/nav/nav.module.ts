import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
// Components
import { MainNavbar } from './nav.component';
import { VisitorNavbar } from './visitor/nav.component';
import { UserNavbar } from './user/nav.component';

// Modules
import { SharedModule } from '../shared/shared.module';
@NgModule({
  declarations: [
		MainNavbar,
		VisitorNavbar,
		UserNavbar,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
		HttpClientModule,
		RouterModule,
		SharedModule,
	],
	exports: [
		MainNavbar,
	],
  providers: [
  ]
})
export class NavigationModule { }
