import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

// Directives 
import { GeoInputDirective } from '../../directives/geo-input.directive';
import { LatInputDirective } from '../../directives/lat-input.directive';

// 
const socketIoConfig: SocketIoConfig = { url: '/chat', options: {} };

@NgModule({
  declarations: [
		GeoInputDirective,
		LatInputDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
		HttpClientModule,
		RouterModule,
		SocketIoModule.forRoot(socketIoConfig),
	],
	exports: [
		GeoInputDirective,
		LatInputDirective,
	],
  providers: [
  ]
})
export class SharedModule { }
