import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs'
import { UserModel } from '../models/user.model'

@Injectable()
export class LoggedInGuardService implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean | Promise<boolean> {
    return new Observable<boolean>((observer) => {
      this.authService.getUser
        .subscribe(
          (user: UserModel) => {
            if (!user) {
              this.router.navigate(['login']);
              observer.next(false);
              observer.complete();
              return;
            }
            observer.next(true);
            observer.complete();
            return;
          }
        );
    });
  }
}
