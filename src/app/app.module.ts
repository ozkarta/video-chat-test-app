import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { Injector, APP_INITIALIZER } from '@angular/core';
import { UserModel } from './shared/models/user.model';

import { AppComponent } from './app.component';

// Router
import { AppRoutingModule } from './app-routing.module';

// Module Imports
import { NavigationModule } from './shared/modules/nav/nav.module';
import { AngularMaterialModule } from './shared/modules/material/mat-import.module';
import { DialogsModule } from './shared/modules/dialogs/dialogs.module';

import { UserModule } from './user/user.module';
import { SharedModule } from './shared/modules/shared/shared.module';

// Components
import { LoginComponent } from './login/login.component';
import { SpinnerComponent } from './shared/components/spinner/spinner.component';
import { RegisterComponent } from './register/register.component';


// Services
import { SpinnerService } from './shared/services/spinner.service';
import { AuthService } from './shared/services/auth.service';

// Interceptors
import { AddHeaderInterceptor } from './shared/interceptors/add-header.interceptor';
// Guards
import { LoggedOutGuardService } from './shared/guards/unauthorized.guard';
import { LoggedInGuardService } from './shared/guards/authorized.guard';

export function initializeApp(authService: AuthService, injector: Injector) {
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      authService.getMe()
        .subscribe(
          (user: UserModel) => {
            return resolve(true);
          },
          (error: Error) => {
            return resolve(false);
          }
        )
    });
  }
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    SpinnerComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule,
    //_________
    SharedModule,
    
    NavigationModule,
    AngularMaterialModule,
    DialogsModule,
    UserModule,

    AppRoutingModule,
  ],
  providers: [
    SpinnerService,
    AuthService,
    // Guards
    LoggedOutGuardService,
    LoggedInGuardService,
    // Interceptors
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AddHeaderInterceptor,
      multi: true,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [AuthService, Injector],
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
