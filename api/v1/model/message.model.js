let mongoose = require('mongoose');

let messageSchema = new mongoose.Schema({
  chat: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Chat',
  },
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  text: {type: String, trim: true}
}, {
  timestamps: true
});

let messageModel = mongoose.model('Message', messageSchema);

exports.model = messageModel;
exports.schema = messageSchema;