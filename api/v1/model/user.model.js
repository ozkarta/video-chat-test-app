let mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
  // SYSTEM FIELDS
  passwordHash: {type: String},
  role: {type: String, enum: ['owner', 'admin', 'client']},
  // USER FIELDS
  firstName: {type: String},
  lastName: {type: String},
  gender: {type: String, enum: ['MALE', 'FEMALE', 'OTHER']},
  birthDate: {type: mongoose.Schema.Types.Date},
  email: {type: String, trim: true},
  friends: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    }
  ],
}, {
  timestamps: true
});

let userModel = mongoose.model('User', userSchema);

exports.model = userModel;
exports.schema = userSchema;