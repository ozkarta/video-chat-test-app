module.exports = function (express) {
  let router = express.Router();
  let ChatModel = require('../../model/chat.model').model;
  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');
  let authGuards = require('../../shared/auth/auth');
  let mongoose = require('mongoose');

  router.get('/:friendId', authGuards.tokenVerificationGuard, async (req, res) => {
    if (!req.userId) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'Token is not valid');
    }

    if (!req.userId || !mongoose.Types.ObjectId.isValid(req.userId)) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You should include VALID userId in request parameters.');
    }

    let friendId = req.params.friendId;

    if (!friendId || !mongoose.Types.ObjectId.isValid(friendId)) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You should include VALID friendId in request parameters.');
    }

    try {
      let chat = await ChatModel.findOne({participants: {$all: [req.userId, friendId]}})
          .populate([
            {
              path: 'messages'
            }
          ])
          .lean()
          .exec();
      return res.status(200).json(chat);
    } catch (error) {
      console.log(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
    

    return res.status(200).json({});
  });

  return router;
}