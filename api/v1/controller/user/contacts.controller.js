module.exports = function (express) {
  let router = express.Router();
  let UserModel = require('../../model/user.model').model;
  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');
  let authGuards = require('../../shared/auth/auth');
  let mongoose = require('mongoose');

  router.get('/', authGuards.tokenVerificationGuard, async (req, res) => {
    if (!req.userId) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'Token is not valid');
    }

    if (!req.userId || !mongoose.Types.ObjectId.isValid(req.userId)) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You should include VALID userId in request parameters.');
    }

    try {
      let friends = [];
      // let user = await UserModel.findById(req.userId)
      //   .populate([
      //     {
      //       path: 'friends',          
      //     }
      //   ])
      //   .lean()
      //   .exec();
      // if (user && user.friends) {
      //   friends = user.friends;
      // }

      friends = await UserModel.find({_id: {$ne: req.userId}}).lean().exec();

      return res.status(200).json(friends);
    } catch (error) {
      console.log(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });

  return router;
}