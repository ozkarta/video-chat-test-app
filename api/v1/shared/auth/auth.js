let jwt = require('jsonwebtoken');
let bcrypt = require('bcryptjs');
let config = require('../../../../config');
let MSG = require('../messages/messages');
let util = require('../util/util');
module.exports.developerAuthGuard = async function(req, res, next) {

};

module.exports.tokenVerificationGuard = async function(req, res, next) {
	let token = req.headers.authorization;
	if (!token) {
		return res.status(MSG.clientError.unAuthorized.code).json(MSG.clientError.unAuthorized);
	}
	jwt.verify(token, config.SECRET, (error, decoded) => {
		if (error) {
			return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, error);
		}
		if (!decoded) {
			return res.status(MSG.clientError.unAuthorized.code).json(MSG.clientError.unAuthorized);
		}
		let userId = decoded.id;
		req.userId = userId;
		return next();
	});
};