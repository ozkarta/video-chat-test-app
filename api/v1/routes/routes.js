module.exports = function (express) {
  var router = express.Router();
  //__________________________________
  
  // shared controllers
  let userController = require('../controller/shared/user.controller.js')(express); 

  // User Controllers
  let userCOntactsController = require('../controller/user/contacts.controller')(express);
  let userChatController = require('../controller/user/chat.controller')(express);
  //_____________SHARED_____________________
  router.use('/shared/user', userController);

  //____________USER_________________________
  router.use('/user/contacts', userCOntactsController);
  router.use('/user/chats', userChatController);
  return router;
};
