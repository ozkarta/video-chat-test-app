let jwt = require('jsonwebtoken');
let config = require('../../config');
let ChatModel = require('../v1/model/chat.model').model;
let UserModel = require('../v1/model/user.model').model;
let MessageModel = require('../v1/model/message.model').model;

module.exports = (socketServer) => {
  let chatSocketServer = socketServer.of('/chat');
  let sockets = {};
  // TODO on destroy => remove socket from object
  chatSocketServer.on('connection', (socket) => {
    socket.emit('we_need_your_token');

    socket.on('you_got_my_token', async (msg) => {
      try {
        let userId = await decodeToken(msg.authorization);
        if (userId) {
          sockets[userId] = socket;
        }
      } catch (error) {
        console.log(error);
      }
    });

    socket.on('message', async (msg) => {

      try {
        let chat = await ChatModel.findOne({participants: {$all: [msg.from, msg.to]}}).lean().exec();
        if (!chat) {
          chat = new ChatModel({
            participants: [
              msg.from, msg.to
            ],
            messages: []
          });
          try {
            chat = await chat.save();
          } catch(error) {
            throw error;
          }
        }
      
        let message = new MessageModel({
          chat: chat,
          sender: msg.from,
          text: msg.message
        });

        
        try {
          await message.save();
          chat = await ChatModel.findOneAndUpdate({_id: chat['_id']}, {$push: {messages: message}}).exec();
          let msgTo = await UserModel.findById(msg.to).exec();
          let msgFrom = await UserModel.findById(msg.from).exec();
          sendMessageToSocket(msg.to, {message: message, chat: chat, user: msgFrom});
          sendMessageToSocket(msg.from, {message: message, chat: chat, user: msgTo});
          
        } catch(error) {
          throw error;
        }
      } catch (error) {
        console.log(error);
      }
    });

    // P2P
    socket.on('I_WANT_P2P', async (msg) => {
      sendGenericResponseToSocket(msg.target, 'NEW_P2P_REQUEST', {from: msg.applicant});
    });

    socket.on('P2P_DENIAL', async (msg) => {
      sendGenericResponseToSocket(msg.applicant, 'P2P_DENIAL', {from: msg.target});
    });

    socket.on('P2P_ACCEPTANCE', async (msg) => {
      sendGenericResponseToSocket(msg.applicant, 'P2P_ACCEPTANCE', {from: msg.target});
    });

    socket.on('P2P_CANCELLATION', async (msg) => {
      console.log(msg);
      sendGenericResponseToSocket(msg.target, 'P2P_CANCELLATION', {from: msg.applicant});
    });

    socket.on('SEND_PEER', async (msg) => {
      sendGenericResponseToSocket(msg.target, 'GET_PEER', {peer: msg.peer});
    });

    function sendGenericResponseToSocket(targetUserId, eventType, data) {
      if (sockets[targetUserId]) {
        if (sockets[targetUserId].connected) {
          sockets[targetUserId].emit(eventType, data);
        } else {
          delete sockets[targetUserId];
        }        
      }
    } 

    function sendMessageToSocket(socketUserId, data) {
      if (sockets[socketUserId]) {
        if (sockets[socketUserId].connected) {
          sockets[socketUserId].emit('message', data);
        } else {
          delete sockets[socketUserId];
        }        
      }
    }
  });
}

function decodeToken(token) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, config.SECRET, (error, decoded) => {
      if (error) {
        return reject(error);
      }
      if (!decoded) {
        return reject(null);
      }
      let userId = decoded.id;
      return resolve(userId);
    });
  });
}